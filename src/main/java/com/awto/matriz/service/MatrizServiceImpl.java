package com.awto.matriz.service;

import com.awto.matriz.model.dto.MatrizDto;
import com.awto.matriz.model.request.MatrizRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class MatrizServiceImpl implements MatrizService{

	@Override
	public ResponseEntity<MatrizDto> getSpiralOrder(MatrizRequest matrizRequest){
		if (validateInput(matrizRequest.getWidth(), matrizRequest.getHeight(),
			matrizRequest.getNumbers().length)){

			MatrizDto matrizDto = MatrizDto.builder().build();
			matrizDto.setMessages("value of fields width and height are different from the size of the array");
			matrizDto.setStatus(HttpStatus.BAD_REQUEST.toString());

			return new ResponseEntity<>(matrizDto, HttpStatus.BAD_REQUEST);
		}
        return ResponseEntity.ok(mapperDto(matrizRequest));
	}

	private Boolean validateInput(Integer width, Integer height , Integer wxh){
		return wxh != width*height || width == 0 || height == 0;
	}

	private Integer[][] converterMatriz(Integer width, Integer height,Integer[] integerList) {
		Integer[][] m = new Integer[width][height];
		int n = 0;
		int i = 0;
		while (i<width){
			for(int j = 0; j< height; j++){
				m[i][j] = integerList[n++];
			}
			i++;
		}
		return m;
	}

	private MatrizDto mapperDto(MatrizRequest matrizRequest) {
		List<Integer> integerArray = Arrays.stream(matrizRequest.getNumbers())
			.collect(Collectors.toList());
		return MatrizDto.builder()
			.sum(sumStatic(integerArray)
				.sum()
			)
			.avg(sumStatic(integerArray)
				.average().isPresent()?
				sumStatic(integerArray)
					.average().getAsDouble():0
			)
			.order(
				spiralConverter(matrizRequest.getWidth(),matrizRequest.getWidth(),
					converterMatriz(matrizRequest.getWidth(), matrizRequest.getHeight(), matrizRequest.getNumbers())
				)
			).build();
	}

	private IntStream sumStatic(List<Integer> list) {
		return list
			.stream()
			.mapToInt(Integer::intValue);
	}

	private List<Integer> spiralConverter(int m, int n, Integer[][] a)
	{
		int i;
		int k = 0;
		int l = 0;
		List<Integer> ls = new ArrayList<>();
		while (k < m && l < n) {
			for (i = l; i < n; ++i) { ls.add(a[k][i]); }
			k++;
			for (i = k; i < m; ++i) { ls.add(a[i][n - 1]); }
			n--;
			if (k < m) {
				for (i = n - 1; i >= l; --i) { ls.add(a[m - 1][i]); }
				m--;
			}
			if (l < n) {
				for (i = m - 1; i >= k; --i) { ls.add(a[i][l]); }
				l++;
			}
		}
		return ls;
	}
}
