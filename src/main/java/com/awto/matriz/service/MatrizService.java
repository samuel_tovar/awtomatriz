package com.awto.matriz.service;

import com.awto.matriz.model.dto.MatrizDto;
import com.awto.matriz.model.request.MatrizRequest;
import org.springframework.http.ResponseEntity;

public interface MatrizService {
	ResponseEntity<MatrizDto> getSpiralOrder(MatrizRequest matrizRequest);
}
