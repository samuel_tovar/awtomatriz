package com.awto.matriz.model.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MatrizDto extends ResponseDto {
	private Integer sum;
	private Double avg;
	private List<Integer> order;
}
