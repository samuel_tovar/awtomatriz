package com.awto.matriz.model.request;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MatrizRequest {
	private Integer width;
	private Integer height;
	private Integer[] numbers;
}
