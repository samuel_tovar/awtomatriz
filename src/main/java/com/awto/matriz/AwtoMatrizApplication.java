package com.awto.matriz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwtoMatrizApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwtoMatrizApplication.class, args);
	}

}
