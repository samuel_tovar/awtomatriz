package com.awto.matriz.controller;

import com.awto.matriz.model.dto.MatrizDto;
import com.awto.matriz.model.request.MatrizRequest;
import com.awto.matriz.service.MatrizService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/awto")
public class MatrizController {

	private final MatrizService matrizService;

	public MatrizController(final MatrizService matrizService) {
		this.matrizService = matrizService;
	}

	@GetMapping("/order")
	public ResponseEntity<MatrizDto> converter(@RequestBody MatrizRequest inputData) {
		System.out.println(inputData);
		return matrizService.getSpiralOrder(inputData);
	}

}
